package de.artemicode.attributtool;

import de.artemicode.attributtool.commands.Help;
import de.artemicode.attributtool.commands.ICommand;

import java.util.Iterator;
import java.util.ServiceLoader;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class AttributTool {

	public static void main(String[] args) {
		if (args.length == 0) {
			Help help = new Help();
			help.execute(new String[0]);
			return;
		}

		ServiceLoader<ICommand> commandServiceLoader = ServiceLoader.load(ICommand.class);
		Iterator<ICommand> commandsIterator = commandServiceLoader.iterator();
		ICommand command = null;

		while (commandsIterator.hasNext()) {
			ICommand thisCommand = commandsIterator.next();
			if (thisCommand.getCommandName().equals(args[0])) {
				command = thisCommand;
				break;
			}
		}

		if (command == null) {
			System.err.println("Unknown command");
			Help help = new Help();
			help.execute(new String[0]);
			return;
		}

		command.execute(args);
	}


}
