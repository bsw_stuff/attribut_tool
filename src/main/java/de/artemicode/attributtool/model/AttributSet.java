package de.artemicode.attributtool.model;

import java.util.ArrayList;
import java.util.List;

public class AttributSet {

	private String charSet;
	private String setName;
	private String language;
	private int attributeCount;

	private final List<AttributCard> cards;

	private final List<String> importWarnings;

	public AttributSet() {
		cards = new ArrayList<>();
		importWarnings = new ArrayList<>();
	}

	public String getCharSet() {
		return charSet;
	}

	public void setCharSet(String charSet) {
		this.charSet = charSet;
	}

	public String getSetName() {
		return setName;
	}

	public void setSetName(String setName) {
		this.setName = setName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getAttributeCount() {
		return attributeCount;
	}

	public void setAttributeCount(int attributeCount) {
		this.attributeCount = attributeCount;
	}

	public List<AttributCard> getCards() {
		return cards;
	}

	public List<String> getImportWarnings() {
		return importWarnings;
	}
}
