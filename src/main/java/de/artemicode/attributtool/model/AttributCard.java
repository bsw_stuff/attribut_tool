package de.artemicode.attributtool.model;

import java.util.ArrayList;
import java.util.List;

public class AttributCard {

	private final List<String> lines;

	public AttributCard() {
		lines = new ArrayList<>();
	}

	public List<String> getLines() {
		return lines;
	}
}
