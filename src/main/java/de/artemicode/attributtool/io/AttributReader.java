package de.artemicode.attributtool.io;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import de.artemicode.attributtool.model.AttributCard;
import de.artemicode.attributtool.model.AttributSet;

public class AttributReader {

	public AttributSet readSet(InputStream inputStream) throws IOException {
		AttributSet set = new AttributSet();
		String charsetName = readFirstLine(inputStream);
		set.setCharSet(charsetName);
		Charset charset = Charset.forName(charsetName);
		try (InputStreamReader reader = new InputStreamReader(inputStream, charset)) {
			try (BufferedReader buffered = new BufferedReader(reader)) {
				String setName = buffered.readLine();
				set.setSetName(setName);
				String langage = buffered.readLine();
				set.setLanguage(langage);
				int count = Integer.valueOf(buffered.readLine());
				set.setAttributeCount(count);

				int phase = 0;
				int index = 0;
				while (true) {
					String line = buffered.readLine();
					if (line == null) {
						break;
					}
					if (line.contains("$$$$")) {
						if (phase != 0) {
							set.getImportWarnings().add("$$$$ marker at card line " + (phase + 1));
						}
						++phase;
						if (index != count) {
							set.getImportWarnings().add("Card line " + phase + " has incorrect count of cards " +
									index + " expected: " + count);
						}
						index = 0;
						continue;
					} else if (line.contains("####")) {
						if (phase != 1) {
							set.getImportWarnings().add("#### marker at card line " + (phase + 1));
						}
						++phase;
						if (index != count) {
							set.getImportWarnings().add("Card line " + phase + " has incorrect count of cards " +
									index + " expected: " + count);
						}
						index = 0;
						continue;
					}
					String[] split = line.split(";");
					for (String section : split) {
						if (section.isEmpty()) {
							continue;
						}
						if (phase == 0) {
							AttributCard card = new AttributCard();
							card.getLines().add(section);
							set.getCards().add(card);
						} else {
							if (index >= set.getCards().size()) {
								AttributCard card = new AttributCard();
								card.getLines().add(section);
								set.getCards().add(card);
								set.getImportWarnings().add("Added additional card at line " + (phase + 1)
										+ " with text " + section);
							} else {
								AttributCard card = set.getCards().get(index);
								card.getLines().add(section);
							}
						}
						index++;
					}
				}
				if (index != count) {
					set.getImportWarnings().add("Card line " + (phase + 1) + " has incorrect count of cards " +
							index + " expected: " + count);
				}
				return set;
			}
		}
	}


	private static String readFirstLine(InputStream stream) throws IOException {
		try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream()) {

			while (true) {
				int in = stream.read();
				if (in == -1) {
					break;
				}
				if (in == 10) {
					stream.mark(1);
					int in2 = stream.read();
					if (in2 == 13) {
						break;
					}
					stream.reset();
					break;
				} else if (in == 13) {
					stream.mark(1);
					int in2 = stream.read();
					if (in2 == 10) {
						break;
					}
					stream.reset();
					break;
				}
				byteOut.write(in);
			}

			byte[] lineBytes = byteOut.toByteArray();

			if ((lineBytes.length >= 3) && (lineBytes[0] == (byte) 0xef)
					&& (lineBytes[1]) == (byte) 0xbB && (lineBytes[2] == (byte) 0xbf)) {
				// BOM ignorieren
				lineBytes = Arrays.copyOfRange(lineBytes, 3, lineBytes.length);
			}

			return new String(lineBytes, StandardCharsets.ISO_8859_1);
		}
	}

}
