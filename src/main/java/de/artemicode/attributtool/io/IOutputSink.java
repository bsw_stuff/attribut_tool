package de.artemicode.attributtool.io;

import java.io.Closeable;
import java.io.IOException;

public interface IOutputSink extends Closeable {

	void println(String line) throws IOException;

}
