package de.artemicode.attributtool.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;

public class FileOutputSink implements IOutputSink {

	private final Writer writer;

	public FileOutputSink(String fileName, Charset encoding) throws FileNotFoundException {
		OutputStream stream = new FileOutputStream(fileName);
		writer = new OutputStreamWriter(stream, encoding);
	}

	@Override
	public void close() throws IOException {
		writer.close();
	}

	@Override
	public void println(String line) throws IOException {
		writer.write(line+"\r\n");
	}

}
