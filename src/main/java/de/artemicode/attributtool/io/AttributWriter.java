package de.artemicode.attributtool.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import de.artemicode.attributtool.model.AttributCard;
import de.artemicode.attributtool.model.AttributSet;
import de.artemicode.attributtool.tools.SetRectifier;

public class AttributWriter {

	public void writeSet(AttributSet set, OutputStream outputStream) throws IOException {
		SetRectifier rectifier = new SetRectifier();
		rectifier.rectifySet(set);

		Charset charset = Charset.forName(set.getCharSet());
		try (OutputStreamWriter writer = new OutputStreamWriter(outputStream, charset)) {
			writer.write(set.getCharSet() + "\r\n");
			writer.write(set.getSetName() + "\r\n");
			writer.write(set.getLanguage() + "\r\n");
			writer.write(set.getAttributeCount() + "\r\n");

			if (set.getAttributeCount() > 0 ) {
				int setLines = set.getCards().get(0).getLines().size();
				for (int lineNumber = 0; lineNumber < setLines; ++lineNumber) {
					if (lineNumber > 0) {
						if (lineNumber == 1) {
							writer.write("$$$$\r\n");
						} else if (lineNumber == 2) {
							writer.write("####\r\n");
						} else {
							throw new IllegalStateException("Cannot output set with more than 3 lines");
						}
					}

					int rows = (set.getCards().size() + 9) / 10;
					for (int row = 0; row < rows; ++ row) {
						StringBuilder line = new StringBuilder();
						for (int col = 0; col < 10; ++ col) {
							int index = row * 10 + col;
							if (index >= set.getCards().size()) {
								continue;
							}
							AttributCard card = set.getCards().get(index);
							line.append(card.getLines().get(lineNumber));
							line.append(';');
						}
						line.append("\r\n");
						writer.write(line.toString());
					}
				}
			}
		}
	}

}
