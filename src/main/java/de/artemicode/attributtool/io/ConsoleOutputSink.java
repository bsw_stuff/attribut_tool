package de.artemicode.attributtool.io;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class ConsoleOutputSink implements IOutputSink {

	@Override
	public void close() {

	}

	@Override
	public void println(String line) {
		System.out.println(line);
	}
}
