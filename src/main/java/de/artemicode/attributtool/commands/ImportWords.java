package de.artemicode.attributtool.commands;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;

import de.artemicode.attributtool.io.AttributWriter;
import de.artemicode.attributtool.model.AttributCard;
import de.artemicode.attributtool.model.AttributSet;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class ImportWords implements ICommand {

	@Override
	public String getCommandName() {
		return "importWords";
	}

	@Override
	public String getDescription() {
		return "Import words from text file into Attribut file";
	}

	@Override
	public String getUsage() {
		return "[inputFile] [attributSetFile] [setName] [language] [charset]";
	}

	@Override
	public void execute(String[] args) {
		if (args.length < 6) {
			System.err.println("importWords [inputFile] [outputFile] [setName] [language] [charSet]");
			return;
		}

		String inputFilename = args[1];
		String outputFilename = args[2];

		String setName = args[3];
		String language = args[4];
		String charSet = args[5];

		AttributSet set = new AttributSet();
		set.setSetName(setName);
		set.setLanguage(language);
		set.setCharSet(charSet);

		try (InputStream inputStream = new FileInputStream(inputFilename)) {
			try (InputStreamReader inputStreamReader =
					new InputStreamReader(inputStream,
							Charset.forName(charSet))) {
				try (BufferedReader bufferedReader =
						new BufferedReader(inputStreamReader)) {

					String line;

					while  ((line = bufferedReader.readLine()) != null) {
						line = line.trim();
						String[] splitted = line.split(";");
						AttributCard card = new AttributCard();
						for (String cardLine : splitted) {
							card.getLines().add(cardLine);
						}
						set.getCards().add(card);
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Input file not found");
			return;
		} catch (IOException e) {
			System.err.println("Error while reading input file");
			return;
		}

		AttributWriter writer = new AttributWriter();
		try (OutputStream outputStream = new FileOutputStream(outputFilename)) {
			writer.writeSet(set, outputStream);
		} catch (IOException e) {
			System.err.println("Error while writing output file");
		}
	}
}
