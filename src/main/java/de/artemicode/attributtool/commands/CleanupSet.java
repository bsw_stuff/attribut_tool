package de.artemicode.attributtool.commands;

import java.io.FileOutputStream;
import java.io.IOException;

import de.artemicode.attributtool.io.AttributWriter;
import de.artemicode.attributtool.model.AttributSet;
import de.artemicode.attributtool.tools.ReadSetTool;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class CleanupSet implements ICommand {

	@Override
	public String getCommandName() {
		return "cleanupSet";
	}

	@Override
	public String getDescription() {
		return "Try to fix errors in the Attribut file - might remove words";
	}

	@Override
	public String getUsage() {
		return "[attributSetFile]";
	}

	@Override
	public void execute(String[] args) {
		if (args.length < 2) {
			System.err.println("No filename given.");
			return;
		}
		AttributSet set = ReadSetTool.readSet(args[1]);
		if (set == null) {
			return;
		}

		try (FileOutputStream outputStream = new FileOutputStream(args[1])) {
			AttributWriter writer = new AttributWriter();
			writer.writeSet(set, outputStream);
		} catch (IOException e) {
			System.err.println("Error occurred while writing file");
		}

	}
}
