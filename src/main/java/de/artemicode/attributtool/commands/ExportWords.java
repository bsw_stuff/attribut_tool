package de.artemicode.attributtool.commands;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;

import de.artemicode.attributtool.io.ConsoleOutputSink;
import de.artemicode.attributtool.io.FileOutputSink;
import de.artemicode.attributtool.io.IOutputSink;
import de.artemicode.attributtool.model.AttributCard;
import de.artemicode.attributtool.model.AttributSet;
import de.artemicode.attributtool.tools.ReadSetTool;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class ExportWords implements ICommand {

	@Override
	public String getCommandName() {
		return "exportWords";
	}

	@Override
	public String getDescription() {
		return "Export the words of a set to a text file";
	}

	@Override
	public String getUsage() {
		return "[attributSetFile] {outputFile}";
	}

	@Override
	public void execute(String[] args) {
		if (args.length < 2) {
			System.err.println("No filename given.");
			return;
		}
		String outFilename = null;
		if (args.length >= 3) {
			outFilename = args[2];
		}

		AttributSet set = ReadSetTool.readSet(args[1]);
		if (set == null) {
			return;
		}

		IOutputSink sink;
		if (outFilename != null) {
			try {
				sink = new FileOutputSink(outFilename, Charset.defaultCharset());
			} catch (FileNotFoundException e) {
				System.err.println("Cannot create output file");
				return;
			}
		} else {
			sink = new ConsoleOutputSink();
		}
		try {

			for (AttributCard card : set.getCards()) {
				StringBuilder cardText = new StringBuilder();
				boolean first = true;
				for (String line : card.getLines()) {
					if (!first) {
						cardText.append(';');
					} else {
						first = false;
					}
					cardText.append(line);
				}
				sink.println(cardText.toString());
			}
		} catch (IOException e) {
			System.err.println("Error while writing words");
		} finally {
			try {
				sink.close();
			} catch (IOException e) {
				System.err.println("Error while writing words");
			}
		}
	}
}
