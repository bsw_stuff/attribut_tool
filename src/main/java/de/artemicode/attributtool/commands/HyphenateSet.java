package de.artemicode.attributtool.commands;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import de.artemicode.attributtool.io.AttributWriter;
import de.artemicode.attributtool.model.AttributCard;
import de.artemicode.attributtool.model.AttributSet;
import de.artemicode.attributtool.tools.CardMeasurer;
import de.artemicode.attributtool.tools.ConsoleTool;
import de.artemicode.attributtool.tools.ReadSetTool;
import de.artemicode.attributtool.tools.split.CardSplitter;
import de.artemicode.attributtool.tools.split.SplitSuggestion;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class HyphenateSet implements ICommand {

	@Override
	public String getCommandName() {
		return "hyphenateSet";
	}

	@Override
	public String getDescription() {
		return "Hyphenate all words which are too large for the card";
	}

	@Override
	public String getUsage() {
		return "[attributSetFile]";
	}

	@Override
	public void execute(String[] args) {
		if (args.length < 2) {
			System.err.println("No filename given.");
			return;
		}
		AttributSet set = ReadSetTool.readSet(args[1]);
		if (set == null) {
			return;
		}

		CardMeasurer measurer = new CardMeasurer();
		int cardNumber = 1;
		for (AttributCard card : set.getCards()) {

			boolean overflow = false;
			for (String line : card.getLines()) {
				if (measurer.getOverflow(line) > 0) {
					overflow = true;
				}
			}
			if (!overflow) {
				cardNumber++;
				continue;
			}

			CardSplitter splitter = new CardSplitter(measurer, set.getLanguage());
			List<SplitSuggestion> suggestions =
					splitter.trySplitCard(card);

			ConsoleTool.newPage();
			System.out.println("Card " + cardNumber);

			System.out.print("\r\n");

			int suggestionNumber = 1;
			for (SplitSuggestion suggestion : suggestions) {
				System.out.println(suggestionNumber + ") " + suggestion);
				suggestionNumber++;
			}
			int selectionInt;
			while (true) {
				try {
					System.out.print("Please select suggestion:");
					System.out.flush();
					String selection = ConsoleTool.readLine();
					selectionInt = Integer.parseInt(selection);
					if (selectionInt < 0 || selectionInt > suggestions.size()) {
						continue;
					}
					System.out.println(selectionInt);
				} catch (IOException | NumberFormatException e) {
					continue;
				}
				break;
			}

			if (selectionInt != 0) {
				SplitSuggestion suggestion = suggestions.get(selectionInt - 1);
				card.getLines().clear();
				card.getLines().addAll(suggestion.getTextLines());
			}

			cardNumber++;
		}

		try (FileOutputStream outputStream = new FileOutputStream(args[1])) {
			AttributWriter writer = new AttributWriter();
			writer.writeSet(set, outputStream);
		} catch (IOException e) {
			System.err.println("Error occurred while writing file");
		}
	}


}
