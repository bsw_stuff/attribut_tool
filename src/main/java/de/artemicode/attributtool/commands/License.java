package de.artemicode.attributtool.commands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class License implements ICommand {

	@Override
	public String getCommandName() {
		return "license";
	}

	@Override
	public String getDescription() {
		return "Show the license information";
	}

	@Override
	public String getUsage() {
		return "{componentName}";
	}

	@Override
	public void execute(String[] args) {

		String licenseFile;

		if (args.length >= 2) {
			licenseFile = "license_" + args[1]+ ".txt";
		} else {
			licenseFile = "license.txt";
		}

		try (InputStream stream = this.getClass().
				getResourceAsStream("/licenses/" + licenseFile)) {
			if (stream == null) {
				return;
			}
			try (InputStreamReader reader = new InputStreamReader(
					stream, StandardCharsets.UTF_8
			)) {
				try (BufferedReader buffered = new BufferedReader(
						reader
				)) {
					String line;

					while ((line = buffered.readLine()) != null) {
						System.out.println(line);
					}
				}
			}
		} catch (IOException e) {
			System.err.println("Cannot output license information");
		}


	}
}
