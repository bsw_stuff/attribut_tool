package de.artemicode.attributtool.tools.split;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.artemicode.attributtool.model.AttributCard;
import de.artemicode.attributtool.tools.CardMeasurer;
import de.mfietz.jhyphenator.HyphenationPattern;
import de.mfietz.jhyphenator.Hyphenator;

public class CardSplitter {

	private final CardMeasurer cardMeasurer;
	private final String language;

	public CardSplitter(CardMeasurer cardMeasurer, String language) {
		this.language = language;
		this.cardMeasurer = cardMeasurer;
	}

	public List<SplitSuggestion> trySplitCard(AttributCard card) {
		StringBuilder connectedText = new StringBuilder();
		for (String line: card.getLines()) {
			if (!line.trim().isEmpty()) {
				if (connectedText.toString().endsWith("-")) {
					connectedText.deleteCharAt(connectedText.length()-1);
				} else {
					if (connectedText.length() != 0 ) {
						connectedText.append(' ');
					}
				}
				connectedText.append(line);
			}
		}
		String connected = connectedText.toString();

		Hyphenator hyphenator = getHyphenator(language);
		List<CardFragment> fragments = splitToFragments(connected);
		List<CardFragment> hypenatedFragments = new ArrayList<>();
		for (CardFragment  fragment : fragments) {
			if (fragment.isTextFragment()) {
				if (hyphenator != null) {
					List<String> hyphenated = hyphenator.hyphenate(fragment.getText());
					for (String syllable : hyphenated) {
						CardFragment syllableFragment =
								new CardFragment(true, syllable);
						hypenatedFragments.add(syllableFragment);
					}
				} else {
					hypenatedFragments.add(fragment);
				}
			} else {
				hypenatedFragments.add(fragment);
			}
		}

		List<SplitSuggestion> suggestions = new ArrayList<>();
		for (int split1 = 1 ; split1 <= hypenatedFragments.size(); ++split1) {
			if (split1 == hypenatedFragments.size()) {
				SplitSuggestion suggestion =
						generateSuggetion(hypenatedFragments, split1, hypenatedFragments.size());
				if (suggestion != null) {
					suggestions.add(suggestion);
				}
			} else {
				for (int split2 = (split1 + 1); split2 <= hypenatedFragments.size(); ++split2) {
					SplitSuggestion suggestion =
							generateSuggetion(hypenatedFragments, split1, split2);
					if (suggestion != null) {
						suggestions.add(suggestion);
					}
				}
			}
		}

		suggestions.sort(new SuggestionComparator());

		int maxHyphens = 3;
		int maxLines = 3;

		Iterator<SplitSuggestion> iterator = suggestions.iterator();
		while (iterator.hasNext()) {
			SplitSuggestion suggestion = iterator.next();

			if (suggestion.getHyphenations() > maxHyphens) {
				iterator.remove();
				continue;
			}

			if (suggestion.getTextLines().size() > maxLines) {
				iterator.remove();
				continue;
			}

			if (!suggestion.hasOverflow()) {
				if (maxHyphens > suggestion.getHyphenations()) {
					maxHyphens = suggestion.getHyphenations();
				}
				if (maxLines > suggestion.getTextLines().size()) {
					maxLines = suggestion.getTextLines().size();
				}
			}
		}

		return suggestions;
	}

	private SplitSuggestion generateSuggetion(List<CardFragment> fragments,
											  int split1, int split2) {
		SplitSuggestion suggestion = new SplitSuggestion();

		if (split1 < fragments.size() && !fragments.get(split1).isTextFragment()) {
			return null;
		}
		if (split2 < fragments.size() && !fragments.get(split2).isTextFragment()) {
			return null;
		}
		boolean hyphenate1 = fragments.get(split1-1).isTextFragment() &&
						split1 < fragments.size()
				&& fragments.get(split1).isTextFragment();
		boolean hyphenate2 = fragments.get(split2-1).isTextFragment() &&
				split2 < fragments.size()
				&& fragments.get(split2).isTextFragment();

		int hyphenations = 0;

		StringBuilder line1 = new StringBuilder();
		for (int i=0;i<split1;++i) {
			CardFragment fragment = fragments.get(i);
			line1.append(fragment.getText());
		}
		if (hyphenate1) {
			line1.append('-');
			hyphenations++;
		}
		suggestion.getTextLines().add(line1.toString().trim());

		if (split1 < fragments.size()) {
			StringBuilder line2 = new StringBuilder();
			for (int i=split1;i<split2;++i) {
				CardFragment fragment = fragments.get(i);
				line2.append(fragment.getText());
			}
			if (hyphenate2) {
				line2.append('-');
				hyphenations++;
			}
			suggestion.getTextLines().add(line2.toString().trim());

			if (split2 < fragments.size()) {
				StringBuilder line3 = new StringBuilder();
				for (int i=split2;i<fragments.size();++i) {
					CardFragment fragment = fragments.get(i);
					line3.append(fragment.getText());
				}
				suggestion.getTextLines().add(line3.toString().trim());
			}
		}
		suggestion.setHyphenations(hyphenations);

		for (String line : suggestion.getTextLines()) {
			int textWidth = cardMeasurer.measure(line);
			suggestion.getOverflowPixels().add(textWidth- 88);
		}

		return suggestion;
	}

	private List<CardFragment> splitToFragments(String text) {
		List<CardFragment> fragments = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		boolean textFragment = false;
		for (int i=0;i<text.length();++i) {
			char c = text.charAt(i);
			boolean thisText = isTextChar(c);
			if (builder.length() == 0) {
				textFragment = thisText;
			} else {
				if (thisText != textFragment) {
					CardFragment fragment = new CardFragment(textFragment,
							builder.toString());
					fragments.add(fragment);
					builder.setLength(0);
					textFragment = thisText;
				}
			}
			builder.append(c);
		}
		if (builder.length() > 0) {
			CardFragment fragment = new CardFragment(textFragment,
					builder.toString());
			fragments.add(fragment);
		}
		return fragments;
	}

	private Hyphenator getHyphenator(String language) {
		HyphenationPattern pattern;
		switch (language) {
			case "de":
				pattern = HyphenationPattern.DE;
				break;
			case "el":	// not supported
				return null;
			case "en":
				pattern = HyphenationPattern.EN_US;
				break;
			case "fi":
				pattern = HyphenationPattern.FI;
				break;
			case "fr":
				pattern = HyphenationPattern.FR;
				break;
			case "it":
				pattern = HyphenationPattern.IT;
				break;
			case "ja":	// not supported
				return null;
			case "nl":
				pattern = HyphenationPattern.NL;
				break;
			case "zh-tr":	// not supported
				return null;
			default:
				return null;
		}
		return Hyphenator.getInstance(pattern);
	}

	private boolean isTextChar(char c) {
		return Character.isLetter(c);
	}

}
