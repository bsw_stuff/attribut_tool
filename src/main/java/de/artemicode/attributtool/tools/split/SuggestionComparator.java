package de.artemicode.attributtool.tools.split;

import java.io.Serializable;
import java.util.Comparator;

public class SuggestionComparator implements Comparator<SplitSuggestion>, Serializable {

	private static final long serialVersionUID = -4540449671182064374L;

	@Override
	public int compare(SplitSuggestion o1, SplitSuggestion o2) {
		boolean overflow1 = o1.hasOverflow();
		boolean overflow2 = o2.hasOverflow();
		if (!overflow1 && overflow2) {
			return -1;
		}
		if (overflow1 && !overflow2) {
			return 1;
		}

		int sum1 = o1.sumOverflow();
		int sum2 = o2.sumOverflow();

		int c = Integer.compare(sum1, sum2);
		if (c != 0) {
			return c;
		}

		c = Integer.compare(o1.getHyphenations(), o2.getHyphenations());
		if (c != 0) {
			return c;
		}

		c = Integer.compare(o1.getTextLines().size(), o2.getTextLines().size());
		if (c != 0) {
			return c;
		}

		return Double.compare(o1.calcVariance(), o2.calcVariance());
	}
}
