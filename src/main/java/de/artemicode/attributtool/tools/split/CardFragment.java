package de.artemicode.attributtool.tools.split;

public class CardFragment {

	private final boolean textFragment;

	private final String text;

	public CardFragment(boolean textFragment, String text) {
		this.textFragment = textFragment;
		this.text = text;
	}

	public boolean isTextFragment() {
		return textFragment;
	}

	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}
}
