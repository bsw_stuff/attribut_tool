package de.artemicode.attributtool.tools.split;

import java.util.ArrayList;
import java.util.List;

public class SplitSuggestion {

	private final List<String> textLines;

	private final List<Integer> overflowPixels;

	private int hyphenations;

	public SplitSuggestion() {
		textLines = new ArrayList<>();
		overflowPixels = new ArrayList<>();
	}

	public List<String> getTextLines() {
		return textLines;
	}

	public List<Integer> getOverflowPixels() {
		return overflowPixels;
	}

	public int getHyphenations() {
		return hyphenations;
	}

	public void setHyphenations(int hyphenations) {
		this.hyphenations = hyphenations;
	}

	public boolean hasOverflow() {
		for (Integer overflow :overflowPixels) {
			if (overflow > 0) {
				return true;
			}
		}
		return false;
	}

	public int sumOverflow() {
		int overflowSum = 0;
		for (Integer overflow :overflowPixels) {
			if (overflow > 0) {
				overflowSum += overflow;
			}
		}
		return overflowSum;
	}

	public double calcVariance() {
		double average = 0;
		for (Integer overflow : overflowPixels) {
			average += overflow;
		}
		average /= overflowPixels.size();

		double variance = 0;
		for (Integer overflow : overflowPixels) {
			double difference  = Math.abs(average - overflow);
			variance = difference * difference;
		}
		variance /= overflowPixels.size();

		return variance;
	}

	public String toString() {
		StringBuilder overflowInfo = new StringBuilder();
		StringBuilder cardText = new StringBuilder();
		boolean first = true;
		int i=0;
		for (String line : textLines) {
			int overflow = overflowPixels.get(i++);
			if (!first) {
				overflowInfo.append('|');
				cardText.append('|');
			} else {
				first = false;
			}
			overflowInfo.append(overflow <= 0 ? "OK" : overflow);
			cardText.append(line);
		}

		return (overflowInfo.toString() + " " + cardText.toString());

	}
}
