package de.artemicode.attributtool.tools;

import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

public class CardMeasurer {

	private final FontRenderContext fontRenderContext;
	private final Font font;

	public CardMeasurer() {
		AffineTransform affineTransform = new AffineTransform();
		fontRenderContext = new FontRenderContext(affineTransform, true, true);
		font = new Font("Helvetica", Font.BOLD, 11);
	}

	public int measure(String text) {
		return (int) (font.getStringBounds(text, fontRenderContext)
				.getWidth());

	}

	public int getCardWidth() {
		return 88;
	}

	public int getOverflow(int width) {
		return width - getCardWidth();
	}

	public int getOverflow(String text) {
		return getOverflow(measure(text));
	}
}
