package de.artemicode.attributtool.tools;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import de.artemicode.attributtool.io.AttributReader;
import de.artemicode.attributtool.model.AttributSet;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class ReadSetTool {

	public static AttributSet readSet(String filename) {
		AttributSet set;
		try (FileInputStream inputStream = new FileInputStream(filename)) {
			AttributReader reader = new AttributReader();
			set = reader.readSet(inputStream);
		} catch (FileNotFoundException e) {
			System.err.println("File does not exist");
			return null;
		} catch (IOException e) {
			System.err.println("Error occured while reading file");
			return null;
		}

		if (!set.getImportWarnings().isEmpty()) {
			System.err.println("--------------IMPORT WARNINGS----------------");
			for (String warning : set.getImportWarnings()) {
				System.err.println(warning);
			}
			System.err.println("--------------IMPORT WARNINGS----------------");
		}
		return set;

	}

}
