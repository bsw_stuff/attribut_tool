package de.artemicode.attributtool.tools;

import java.io.IOException;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class ConsoleTool {

	public static void newPage() {
		StringBuilder builder = new StringBuilder();
		for (int i=0;i<50;++i) {
			builder.append("\r\n");
		}
		System.out.print(builder.toString());
	}

	public static String readLine() throws IOException {
		StringBuilder builder = new StringBuilder();

		while (true) {
			int in = System.in.read();
			if (in == -1) {
				break;
			}
			if (in == 10) {
				break;
			}
			if (in == 13) {
				continue;
			}
			builder.append((char) in);
		}
		return builder.toString();
	}
}
