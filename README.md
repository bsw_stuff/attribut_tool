Attribut tool
=============

Compiling
---------

Windows:

gradlew.bat shadowJar

Linux / Unix:

./gradlew shadowJar

**A .jar file will be generated in "build/libs" which is executable.**


Usage
-----

Type:

java -jar attribut_tool-1.0.jar help

for general instructions

